# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/definition')

RSpec.describe(Defmastership::Definition) do
  describe '.new' do
    context 'when minimal' do
      subject do
        described_class.new(
          type: 'req',
          reference: 'TUTU-001'
        )
      end

      it { is_expected.not_to(be_nil) }
      it { is_expected.to(have_attributes(type:       'req')) }
      it { is_expected.to(have_attributes(reference:  'TUTU-001')) }
      it { is_expected.to(have_attributes(lines:      [])) }
      it { is_expected.to(have_attributes(value:      '')) }
      it { is_expected.to(have_attributes(eref:       {})) }
      it { is_expected.to(have_attributes(iref:       [])) }
      it { is_expected.to(have_attributes(attributes: {})) }
      it { is_expected.to(have_attributes(labels:     Set.new)) }
      it { is_expected.to(have_attributes(wrong_explicit_checksum: nil)) }
      it { is_expected.to(have_attributes(explicit_version: nil)) }
    end

    context 'with labels' do
      subject do
        described_class.new(
          type: 'req',
          reference: 'TUTU-001',
          labels: 'something'
        )
      end

      it { is_expected.to(have_attributes(labels: Set['something'])) }
    end

    context 'with explicit_checksum' do
      subject(:definition) do
        described_class.new(
          type: 'req',
          reference: 'TUTU-001',
          explicit_checksum: '~8cc259e6'
        )
      end

      it do
        definition << 'def value with a checksum != ~8cc259e6'
        expect(definition).to(have_attributes(wrong_explicit_checksum: '~8cc259e6'))
      end

      it do
        definition << 'def value with a good checksum'
        expect(definition).to(have_attributes(wrong_explicit_checksum: nil))
      end
    end

    context 'with explicit_version' do
      subject(:definition) do
        described_class.new(
          type: 'req',
          reference: 'TUTU-001',
          explicit_version: 'pouet'
        )
      end

      it do
        expect(definition).to(have_attributes(explicit_version: 'pouet'))
      end
    end
  end

  describe '#<<' do
    subject(:definition) do
      described_class.new(
        type: 'req',
        reference: 'TUTU-001'
      )
    end

    it 'has value when one line is added' do
      definition << 'first line'
      expect(definition.value).to(eq('first line'))
    end

    it 'stores line when added' do
      definition << 'first line'
      expect(definition.lines).to(eq(['first line']))
    end

    it 'has multiline value when multiple lines are added' do
      definition << 'first line' << 'second line'
      expect(definition.value).to(eq("first line\nsecond line"))
    end

    it 'stores each line when added' do
      definition << 'first line' << 'second line'
      expect(definition.lines).to(eq(['first line', 'second line']))
    end

    it 'calculates sha256_short of value' do
      definition << 'first line' << 'second line'
      expect(definition.sha256_short).to(eq('~beb0535a'))
    end

    it 'returns the definition and not the "line"' do
      expect { (definition << 'first line').sha256_short }
        .not_to(raise_error)
    end
  end

  describe '#sha256_short' do
    context 'when definition has no summary' do
      subject(:definition) { described_class.new(type: 'req') }

      it 'calculates sha256_short of value' do
        definition << 'first line' << 'second line'
        expect(definition.sha256_short).to(eq('~beb0535a'))
      end
    end

    context 'when definition has a summary' do
      subject(:definition) { described_class.new(type: 'req', summary: 'first line') }

      it 'calculates sha256_short of value' do
        definition << 'second line'
        expect(definition.sha256_short).to(eq('~beb0535a'))
      end
    end
  end

  describe '#add_eref' do
    subject(:definition) do
      described_class.new(
        type: 'req',
        reference: 'TUTU-001'
      )
    end

    [
      'tutu,titi,pouet',
      '   tutu    ,  titi  ,   pouet   '
    ].each do |erefs_cs|
      it 'shall accept to add comma separated erefs' do
        definition.add_eref(:foo, erefs_cs)
        expect(definition.eref[:foo]).to(eq(%w[tutu titi pouet]))
      end
    end
  end

  describe '#add_iref' do
    subject(:definition) do
      described_class.new(
        type: 'req',
        reference: 'TUTU-001'
      )
    end

    it 'shall accept to add multiple iref' do
      definition.add_iref('toto')
      definition.add_iref('tutu')
      expect(definition.iref).to(eq(%w[toto tutu]))
    end
  end

  describe '#set_attribute' do
    subject(:definition) do
      described_class.new(
        type: 'req',
        reference: 'TUTU-001'
      )
    end

    it 'shall accept to set attribute' do
      definition.set_attribute(:whatever, 'value')
      expect(definition.attributes[:whatever]).to(eq('value'))
    end
  end
end
