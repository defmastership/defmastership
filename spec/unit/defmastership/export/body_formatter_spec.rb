# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/export/body_formatter')

RSpec.describe(Defmastership::Export::BodyFormatter) do
  subject(:formatter) { described_class.new(document, definition) }

  let(:document)   { instance_double(Defmastership::Document, 'document')     }
  let(:definition) { instance_double(Defmastership::Definition, 'definition') }

  describe '.new' do
    it { is_expected.not_to(be_nil) }
  end

  describe '#type' do
    before do
      allow(definition).to(receive(:type).with(no_args).and_return('a'))
      formatter.type
    end

    it { expect(definition).to(have_received(:type).with(no_args)) }
    it { expect(formatter.type).to(eq(['a'])) }
  end

  describe '#reference' do
    before do
      allow(definition).to(receive(:reference).with(no_args).and_return('b'))
      formatter.reference
    end

    it { expect(definition).to(have_received(:reference).with(no_args)) }
    it { expect(formatter.reference).to(eq(['b'])) }
  end

  describe '#summary' do
    before do
      allow(definition).to(receive(:summary).with(no_args).and_return('b'))
      formatter.summary
    end

    it { expect(definition).to(have_received(:summary).with(no_args)) }
    it { expect(formatter.summary).to(eq(['b'])) }
  end

  describe '#value' do
    before do
      allow(definition).to(receive(:value).with(no_args).and_return('c'))
      formatter.value
    end

    it { expect(definition).to(have_received(:value).with(no_args)) }
    it { expect(formatter.value).to(eq(['c'])) }
  end

  describe '#checksum' do
    before do
      allow(definition).to(receive(:sha256_short).with(no_args).and_return('d'))
      formatter.checksum
    end

    it { expect(definition).to(have_received(:sha256_short).with(no_args)) }
    it { expect(formatter.checksum).to(eq(['d'])) }
  end

  describe '#wrong_explicit_checksum' do
    context 'when no wrong_explicit checksum' do
      before do
        allow(definition).to(receive(:wrong_explicit_checksum).with(no_args).and_return(nil))
        formatter.wrong_explicit_checksum
      end

      it { expect(definition).to(have_received(:wrong_explicit_checksum).with(no_args)) }
      it { expect(formatter.wrong_explicit_checksum).to(eq([''])) }
    end

    context 'when explicit checksum' do
      before do
        allow(definition).to(receive(:wrong_explicit_checksum).with(no_args).and_return('ab12'))
        formatter.wrong_explicit_checksum
      end

      it { expect(formatter.wrong_explicit_checksum).to(eq(['ab12'])) }
    end
  end

  describe '#explicit_version' do
    context 'when no explicit_version' do
      before do
        allow(definition).to(receive(:explicit_version).with(no_args).and_return(nil))
        formatter.explicit_version
      end

      it { expect(definition).to(have_received(:explicit_version).with(no_args)) }
      it { expect(formatter.explicit_version).to(eq([''])) }
    end

    context 'when explicit_version' do
      before do
        allow(definition).to(receive(:explicit_version).with(no_args).and_return('ab12'))
        formatter.explicit_version
      end

      it { expect(formatter.explicit_version).to(eq(['ab12'])) }
    end
  end

  describe '#labels' do
    before { allow(document).to(receive(:labels).with(no_args).and_return(['whatever'])) }

    context 'when no labels on definition' do
      before do
        allow(definition).to(receive(:labels).with(no_args).and_return([]))
        formatter.labels
      end

      it { expect(definition).to(have_received(:labels).once.with(no_args)) }
      it { expect(formatter.labels).to(eq([''])) }
    end

    context 'when labels on definition' do
      before do
        allow(definition).to(receive(:labels).with(no_args).and_return(%w[toto tutu].to_set))
        formatter.labels
      end

      it { expect(definition).to(have_received(:labels).once.with(no_args)) }
      it { expect(formatter.labels).to(eq(["toto\ntutu"])) }
    end
  end

  describe '#eref' do
    context 'when no eref on the document' do
      before do
        allow(document).to(receive(:eref).with(no_args).and_return({}))
        formatter.eref
      end

      it { expect(document).to(have_received(:eref).with(no_args)) }
      it { expect(formatter.eref).to(eq([])) }
    end

    context 'when eref on the document' do
      before do
        allow(document).to(
          receive(:eref).with(no_args).and_return(
            a: 'whatever', b: 'whatever', c: 'whatever'
          )
        )
        allow(definition).to(receive(:eref).with(no_args).and_return(a: %w[A B], b: [], c: ['C']))
        formatter.eref
      end

      it { expect(definition).to(have_received(:eref).exactly(3).times.with(no_args)) }
      it { expect(formatter.eref).to(eq(["A\nB", '', 'C'])) }
    end

    context 'when missing eref on the definition' do
      before do
        allow(document).to(
          receive(:eref).with(no_args).and_return(
            a: 'whatever', b: 'whatever'
          )
        )
        allow(definition).to(receive(:eref).with(no_args).and_return(b: ['B']))
        formatter.eref
      end

      it { expect(definition).to(have_received(:eref).exactly(2).times.with(no_args)) }
      it { expect(formatter.eref).to(eq(['', 'B'])) }
    end
  end

  describe '#iref' do
    before { allow(document).to(receive(:iref).with(no_args).and_return(true)) }

    context 'when no iref on the definition' do
      before do
        allow(definition).to(receive(:iref).with(no_args).and_return([]))
        formatter.iref
      end

      it { expect(definition).to(have_received(:iref).with(no_args)) }
      it { expect(formatter.iref).to(eq([''])) }
    end

    context 'when iref on the definition' do
      before do
        allow(definition).to(receive(:iref).with(no_args).and_return(%w[A B]))
        formatter.iref
      end

      it { expect(definition).to(have_received(:iref).with(no_args)) }
      it { expect(formatter.iref).to(eq(["A\nB"])) }
    end
  end

  describe '#attributes' do
    context 'when no attributes on the document' do
      before do
        allow(document).to(receive(:attributes).with(no_args).and_return({}))
        formatter.attributes
      end

      it { expect(document).to(have_received(:attributes).with(no_args)) }
      it { expect(formatter.attributes).to(eq([])) }
    end

    context 'when attributes on the document' do
      before do
        allow(document).to(
          receive(:attributes).with(no_args)
            .and_return(a: 'whatever', b: 'whatever', c: 'whatever')
        )
        allow(definition).to(receive(:attributes).and_return(a: 'X', b: '', c: 'Y'))
        formatter.attributes
      end

      it { expect(definition).to(have_received(:attributes).exactly(3).times) }
      it { expect(formatter.attributes).to(eq(['X', '', 'Y'])) }
    end

    context 'when miing attribute on the definition' do
      before do
        allow(document).to(
          receive(:attributes).with(no_args)
            .and_return(a: 'whatever', b: 'whatever', c: 'whatever')
        )
        allow(definition).to(receive(:attributes).and_return(a: 'X', b: ''))
        formatter.attributes
      end

      it { expect(definition).to(have_received(:attributes).exactly(3).times) }
      it { expect(formatter.attributes).to(eq(['X', '', ''])) }
    end
  end
end
