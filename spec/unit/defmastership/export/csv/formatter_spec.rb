# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('csv')
require('defmastership/export/csv/formatter')
require('ostruct')

RSpec.describe(Defmastership::Export::CSV::Formatter) do
  subject(:formatter) { described_class.new(document, ';') }

  let(:document) { instance_double(Defmastership::Document, 'document') }

  describe '.new' do
    it { is_expected.not_to(be_nil) }
  end

  describe '#export_to' do
    let(:header) { instance_double(Defmastership::Export::HeaderFormatter, 'header') }
    let(:bodies) do
      [
        instance_double(Defmastership::Export::BodyFormatter, 'bodies[0]'),
        instance_double(Defmastership::Export::BodyFormatter, 'bodies[1]')
      ]
    end
    let(:csv) { instance_double(CSV, 'csv') }

    before do
      allow(CSV).to(receive(:open).with('whatever', 'w:ISO-8859-1', col_sep: ';').and_yield(csv))
      allow(csv).to(receive(:<<))
      allow(Defmastership::Export::HeaderFormatter).to(receive(:new).with(document).and_return(header))
      bodies.each_with_index do |body, index|
        allow(Defmastership::Export::BodyFormatter).to(
          receive(:new).with(
            document,
            :"def#{index}"
          ).and_return(body)
        )
      end
      allow(document).to(receive(:definitions).and_return(%i[def0 def1]))
      allow(document).to(receive(:summaries?).with(no_args).and_return(false))
      allow(document).to(receive(:wrong_explicit_checksum?).with(no_args).and_return(false))
      allow(document).to(receive(:explicit_version?).with(no_args).and_return(false))
      allow(document).to(receive(:labels).with(no_args).and_return([]))
      allow(document).to(receive(:eref).with(no_args).and_return([]))
      allow(document).to(receive(:iref).with(no_args).and_return(false))
      allow(document).to(receive(:attributes).with(no_args).and_return([]))
    end

    context 'when no variable columns' do
      methods = %i[type reference value checksum]

      before do
        methods.each do |method|
          allow(header).to(receive(method).with(no_args).and_return(["#{method} header"]))
          bodies.each_with_index do |body, index|
            allow(body).to(receive(method).with(no_args).and_return(["#{method} def#{index} body"]))
          end
        end
        formatter.export_to('whatever')
      end

      it { expect(CSV).to(have_received(:open).with('whatever', 'w:ISO-8859-1', col_sep: ';')) }

      methods.each do |method|
        it { expect(header).to(have_received(method).with(no_args)) }

        2.times do |index|
          it { expect(bodies[index]).to(have_received(method).with(no_args)) }
        end
      end

      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} header" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def0 body" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def1 body" })) }
    end

    context 'when summary' do
      methods = %i[type reference summary value checksum]

      before do
        methods.each do |method|
          allow(header).to(receive(method).with(no_args).and_return(["#{method} header"]))
          bodies.each_with_index do |body, index|
            allow(body).to(receive(method).with(no_args).and_return(["#{method} def#{index} body"]))
          end
        end
        allow(document).to(receive(:summaries?).with(no_args).and_return([:whatever]))
        formatter.export_to('whatever')
      end

      methods.each do |method|
        it { expect(header).to(have_received(method).with(no_args)) }

        2.times do |index|
          it { expect(bodies[index]).to(have_received(method).with(no_args)) }
        end
      end

      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} header" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def0 body" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def1 body" })) }
    end

    context 'when wrong_explicit_checksum' do
      methods = %i[type reference value checksum wrong_explicit_checksum]

      before do
        methods.each do |method|
          allow(header).to(receive(method).with(no_args).and_return(["#{method} header"]))
          bodies.each_with_index do |body, index|
            allow(body).to(receive(method).with(no_args).and_return(["#{method} def#{index} body"]))
          end
        end
        allow(document).to(receive(:wrong_explicit_checksum?).with(no_args).and_return(true))
        formatter.export_to('whatever')
      end

      methods.each do |method|
        it { expect(header).to(have_received(method).with(no_args)) }

        2.times do |index|
          it { expect(bodies[index]).to(have_received(method).with(no_args)) }
        end
      end

      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} header" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def0 body" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def1 body" })) }
    end

    context 'when explicit_version' do
      methods = %i[type reference value checksum explicit_version]

      before do
        methods.each do |method|
          allow(header).to(receive(method).with(no_args).and_return(["#{method} header"]))
          bodies.each_with_index do |body, index|
            allow(body).to(receive(method).with(no_args).and_return(["#{method} def#{index} body"]))
          end
        end
        allow(document).to(receive(:explicit_version?).with(no_args).and_return(true))
        formatter.export_to('whatever')
      end

      methods.each do |method|
        it { expect(header).to(have_received(method).with(no_args)) }

        2.times do |index|
          it { expect(bodies[index]).to(have_received(method).with(no_args)) }
        end
      end

      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} header" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def0 body" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def1 body" })) }
    end

    context 'when labels' do
      methods = %i[type reference value checksum labels]

      before do
        methods.each do |method|
          allow(header).to(receive(method).with(no_args).and_return(["#{method} header"]))
          bodies.each_with_index do |body, index|
            allow(body).to(receive(method).with(no_args).and_return(["#{method} def#{index} body"]))
          end
        end
        allow(document).to(receive(:labels).with(no_args).and_return([:whatever]))
        formatter.export_to('whatever')
      end

      methods.each do |method|
        it { expect(header).to(have_received(method).with(no_args)) }

        2.times do |index|
          it { expect(bodies[index]).to(have_received(method).with(no_args)) }
        end
      end

      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} header" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def0 body" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def1 body" })) }
    end

    context 'when eref' do
      methods = %i[type reference value checksum eref]

      before do
        methods.each do |method|
          allow(header).to(receive(method).with(no_args).and_return(["#{method} header"]))
          bodies.each_with_index do |body, index|
            allow(body).to(receive(method).with(no_args).and_return(["#{method} def#{index} body"]))
          end
        end
        allow(document).to(receive(:eref).with(no_args).and_return([:whatever]))
        formatter.export_to('whatever')
      end

      methods.each do |method|
        it { expect(header).to(have_received(method).with(no_args)) }

        2.times do |index|
          it { expect(bodies[index]).to(have_received(method).with(no_args)) }
        end
      end

      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} header" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def0 body" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def1 body" })) }
    end

    context 'when iref' do
      methods = %i[type reference value checksum iref]

      before do
        methods.each do |method|
          allow(header).to(receive(method).with(no_args).and_return(["#{method} header"]))
          bodies.each_with_index do |body, index|
            allow(body).to(receive(method).with(no_args).and_return(["#{method} def#{index} body"]))
          end
        end
        allow(document).to(receive(:iref).with(no_args).and_return(true))
        formatter.export_to('whatever')
      end

      methods.each do |method|
        it { expect(header).to(have_received(method).with(no_args)) }

        2.times do |index|
          it { expect(bodies[index]).to(have_received(method).with(no_args)) }
        end
      end

      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} header" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def0 body" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def1 body" })) }
    end

    context 'when attributes' do
      methods = %i[type reference value checksum attributes]

      before do
        methods.each do |method|
          allow(header).to(receive(method).with(no_args).and_return(["#{method} header"]))
          bodies.each_with_index do |body, index|
            allow(body).to(receive(method).with(no_args).and_return(["#{method} def#{index} body"]))
          end
        end
        allow(document).to(receive(:attributes).with(no_args).and_return([:whatever]))
        formatter.export_to('whatever')
      end

      methods.each do |method|
        it { expect(header).to(have_received(method).with(no_args)) }

        2.times do |index|
          it { expect(bodies[index]).to(have_received(method).with(no_args)) }
        end
      end

      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} header" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def0 body" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def1 body" })) }
    end

    context 'when every colums' do
      methods = %i[
        type
        reference
        summary
        value
        checksum
        wrong_explicit_checksum
        explicit_version
        labels
        eref
        iref
        attributes
      ]

      before do
        methods.each do |method|
          allow(header).to(receive(method).with(no_args).and_return(["#{method} header"]))
          bodies.each_with_index do |body, index|
            allow(body).to(receive(method).with(no_args).and_return(["#{method} def#{index} body"]))
          end
        end
        allow(document).to(receive(:summaries?).with(no_args).and_return(true))
        allow(document).to(receive(:wrong_explicit_checksum?).with(no_args).and_return(true))
        allow(document).to(receive(:explicit_version?).with(no_args).and_return(true))
        allow(document).to(receive(:labels).with(no_args).and_return([:whatever]))
        allow(document).to(receive(:eref).with(no_args).and_return([:whatever]))
        allow(document).to(receive(:iref).with(no_args).and_return(true))
        allow(document).to(receive(:attributes).with(no_args).and_return([:whatever]))
        formatter.export_to('whatever')
      end

      methods.each do |method|
        it { expect(header).to(have_received(method).with(no_args)) }

        2.times do |index|
          it { expect(bodies[index]).to(have_received(method).with(no_args)) }
        end
      end

      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} header" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def0 body" })) }
      it { expect(csv).to(have_received(:<<).with(methods.map { |method| "#{method} def1 body" })) }
    end
  end
end
