# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/export/header_formatter')

RSpec.describe(Defmastership::Export::HeaderFormatter) do
  subject(:formatter) { described_class.new(document) }

  let(:document) { instance_double(Defmastership::Document, 'document') }

  describe '.new' do
    it { is_expected.not_to(be_nil) }
  end

  describe '#header' do
    describe '#type' do
      it { expect(formatter.type).to(eq(['Type'])) }
    end

    describe '#reference' do
      it { expect(formatter.reference).to(eq(['Reference'])) }
    end

    describe '#summary' do
      it { expect(formatter.summary).to(eq(['Summary'])) }
    end

    describe '#value' do
      it { expect(formatter.value).to(eq(['Value'])) }
    end

    describe '#checksum' do
      it { expect(formatter.checksum).to(eq(['Checksum'])) }
    end

    describe '#explicit_version' do
      context 'when no explicit version' do
        before do
          allow(document).to(receive(:explicit_version?).and_return(false))
          formatter.explicit_version
        end

        it { expect(document).to(have_received(:explicit_version?)) }

        it { expect(formatter.explicit_version).to(eq([])) }
      end

      context 'when explicit version' do
        before do
          allow(document).to(receive(:explicit_version?).and_return(true))
          formatter.explicit_version
        end

        it { expect(formatter.explicit_version).to(eq(['Version'])) }
      end
    end

    describe '#wrong_explicit_checksum' do
      context 'when no wrong explicit checksum' do
        before do
          allow(document).to(receive(:wrong_explicit_checksum?).and_return(false))
          formatter.wrong_explicit_checksum
        end

        it { expect(document).to(have_received(:wrong_explicit_checksum?)) }

        it { expect(formatter.wrong_explicit_checksum).to(eq([])) }
      end

      context 'when wrong explicit checksum' do
        before do
          allow(document).to(receive(:wrong_explicit_checksum?).and_return(true))
          formatter.wrong_explicit_checksum
        end

        it { expect(formatter.wrong_explicit_checksum).to(eq(['Wrong explicit checksum'])) }
      end
    end

    describe '#labels' do
      context 'when no labels' do
        before do
          allow(document).to(receive(:labels).and_return([]))
          formatter.labels
        end

        it { expect(document).to(have_received(:labels)) }

        it { expect(formatter.labels).to(eq([])) }
      end

      context 'when there is labels' do
        before { allow(document).to(receive(:labels).and_return(['Whatever'])) }

        it { expect(formatter.labels).to(eq(['Labels'])) }
      end
    end

    describe '#eref' do
      context 'when no eref' do
        before do
          allow(document).to(receive(:eref).and_return({}))
          formatter.eref
        end

        it { expect(document).to(have_received(:eref)) }

        it { expect(formatter.eref).to(eq([])) }
      end

      context 'when eref' do
        before do
          allow(document).to(
            receive(:eref).and_return(
              whatever: { prefix: 'A' },
              other: { prefix: 'C', url: 'D' }
            )
          )
        end

        it { expect(formatter.eref).to(eq(%w[A C])) }
      end
    end

    describe '#iref' do
      context 'when no iref' do
        before do
          allow(document).to(receive(:iref).and_return(false))
          formatter.iref
        end

        it { expect(document).to(have_received(:iref)) }

        it { expect(formatter.iref).to(eq([])) }
      end

      context 'when iref' do
        before { allow(document).to(receive(:iref).and_return(true)) }

        it { expect(formatter.iref).to(eq(['Internal links'])) }
      end
    end

    describe '#attributes' do
      context 'when no attributes' do
        before do
          allow(document).to(receive(:attributes).and_return({}))
          formatter.attributes
        end

        it { expect(document).to(have_received(:attributes)) }

        it { expect(formatter.attributes).to(eq([])) }
      end

      context 'when attributes' do
        before { allow(document).to(receive(:attributes).and_return(whatever: 'A', other: 'B')) }

        it { expect(formatter.attributes).to(eq(%w[A B])) }
      end
    end
  end
end
