# Copyright (c) 2024 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/matching_line')

RSpec.describe(Defmastership::MatchingLine) do
  describe '.new' do
    subject(:matching_line) do
      described_class.new(:a_match, :a_line)
    end

    it { is_expected.not_to(be_nil) }
    it { is_expected.to(have_attributes(match: :a_match)) }
    it { is_expected.to(have_attributes(line: :a_line)) }
  end

  describe '#[]' do
    subject(:matching_line) do
      described_class.new(match, :a_line)
    end

    let(:match) { instance_double(Hash, 'match') }

    before do
      allow(match).to(receive(:[]).with(:whatever).and_return('Whatever'))
      allow(match).to(receive(:[]).with(:line).and_return(nil))
    end

    it do
      matching_line[:whatever]
      expect(match).to(have_received(:[]).with(:whatever))
    end

    it { expect(matching_line[:whatever]).to(eq('Whatever')) }
    it { expect(matching_line[:line]).to(eq(:a_line)) }
  end
end
