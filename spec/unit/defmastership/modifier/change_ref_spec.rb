# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/modifier/change_ref')

RSpec.describe(Defmastership::Modifier::ChangeRef) do
  subject(:refchanger) { described_class.new({}) }

  describe '.new' do
    it { expect(described_class.ancestors).to(include(Defmastership::Modifier::ModifierCommon)) }
    it { is_expected.not_to(be_nil) }
    it { is_expected.to(have_attributes(from_regexp: '')) }
    it { is_expected.to(have_attributes(to_template: '')) }
    it { is_expected.to(have_attributes(next_ref:    0)) }
  end

  describe '.replacement_methods' do
    it { expect(described_class.replacement_methods).to(eq(%i[replace_refdef replace_irefs])) }
  end

  describe '#config' do
    context 'when not initalized' do
      it do
        expect(refchanger.config).to(
          include(from_regexp: '', to_template: '', next_ref: 0)
        )
      end
    end
  end

  describe '#replace_refdef' do
    context 'when really simple rule' do
      subject(:refchanger) do
        described_class.new(
          from_regexp: 'TEMP',
          to_template: 'TUTU'
        )
      end

      context 'when valid definition' do
        it do
          expect(refchanger.replace_refdef('[define, whatever, TEMP]'))
            .to(eq('[define, whatever, TUTU]'))
        end

        it do
          expect(refchanger.replace_refdef('[define, whatever, TEMP(a~12345678)]'))
            .to(eq('[define, whatever, TUTU(a~12345678)]'))
        end

        it do
          expect(refchanger.replace_refdef('[define, whatever, TEMP(~12345678)]'))
            .to(eq('[define, whatever, TUTU(~12345678)]'))
        end

        it do
          expect(refchanger.replace_refdef('[define, whatever, TEMP(a)]'))
            .to(eq('[define, whatever, TUTU(a)]'))
        end
      end

      context 'when no valid definition' do
        it do
          expect(refchanger.replace_refdef('[pouet, whatever, TEMP]'))
            .to(eq('[pouet, whatever, TEMP]'))
        end
      end
    end

    context 'when template is variable' do
      subject(:refchanger) do
        described_class.new(
          from_regexp: 'TEMP',
          to_template: 'TOTO-%<next_ref>04d',
          next_ref: 124
        )
      end

      it do
        expect(refchanger.replace_refdef('[define, whatever, TEMP]'))
          .to(eq('[define, whatever, TOTO-0124]'))
      end

      it do
        refchanger.replace_refdef('[define, whatever, TEMP]')
        expect(refchanger).to(have_attributes(next_ref: 125))
      end
    end

    context 'when capture group in regexp' do
      subject(:refchanger) do
        described_class.new(
          from_regexp: '(?<cg>TOTO|TITI)-TEMP[\d]*',
          to_template: '%<cg>s-%<next_ref>04d',
          next_ref: 132
        )
      end

      it do
        expect(refchanger.replace_refdef('[define, whatever, TOTO-TEMP]'))
          .to(eq('[define, whatever, TOTO-0132]'))
      end

      it do
        refchanger.replace_refdef('[define, whatever, TOTO-TEMP]')
        expect(refchanger.replace_refdef('[define, whatever, TITI-TEMP]'))
          .to(eq('[define, whatever, TITI-0133]'))
      end

      it do
        refchanger.replace_refdef('[define, whatever, TOTO-TEMP1]')
        refchanger.replace_refdef('[define, whatever, TITI-TEMP2]')
        expect(refchanger).to(have_attributes(changes: [%w[TOTO-TEMP1 TOTO-0132], %w[TITI-TEMP2 TITI-0133]]))
      end
    end

    context 'when definition is in literal block' do
      subject(:refchanger) do
        described_class.new(
          from_regexp: 'TEMP',
          to_template: 'TUTU'
        )
      end

      before do
        refchanger.replace_refdef('....')
      end

      it do
        expect(refchanger.replace_refdef('[define, whatever, TEMP]'))
          .to(eq('[define, whatever, TEMP]'))
      end
    end

    context 'when defintion is after literal block' do
      subject(:refchanger) do
        described_class.new(
          from_regexp: 'TEMP',
          to_template: 'TUTU'
        )
      end

      before do
        refchanger.replace_refdef("....\n")
        refchanger.replace_refdef('[define, whatever, TEMP]')
        refchanger.replace_refdef("....\n")
      end

      it do
        expect(refchanger.replace_refdef('[define, whatever, TEMP]'))
          .to(eq('[define, whatever, TUTU]'))
      end
    end
  end

  describe '#replace_irefs' do
    subject(:refchanger) do
      described_class.new(
        from_regexp: '(?<cg>TOTO|TITI)-TEMP[\d]*',
        to_template: '%<cg>s-%<next_ref>04d',
        next_ref: 132
      )
    end

    before do
      refchanger.replace_refdef('[define, whatever, TOTO-TEMP123]')
      refchanger.replace_refdef('[define, whatever, TITI-TEMP421]')
    end

    it do
      expect(refchanger.replace_irefs('defs:iref[TOTO-TEMP1234]'))
        .to(eq('defs:iref[TOTO-TEMP1234]'))
    end

    it do
      expect(refchanger.replace_irefs('defs:iref[TOTO-TEMP123]'))
        .to(eq('defs:iref[TOTO-0132]'))
    end

    it do
      expect(
        refchanger.replace_irefs(
          'defs:iref[TOTO-TEMP123] defs:iref[TITI-TEMP421] bla'
        )
      ).to(eq('defs:iref[TOTO-0132] defs:iref[TITI-0133] bla'))
    end

    it do
      expect(
        refchanger.replace_irefs(
          'defs:iref[TOTO-TEMP123] defs:iref[TOTO-TEMP123] bla'
        )
      ).to(eq('defs:iref[TOTO-0132] defs:iref[TOTO-0132] bla'))
    end
  end
end
