# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/modifier/update_def')

module Defmastership
  module Modifier
    # Just a class for tests
    class ConcreteRef < UpdateDef
      private

      # This method smells of :reek:UtilityFunction
      def reference_replacement(reference, match)
        "#{reference}_something_#{match[:explicit_version]}"
      end
    end
  end
end

RSpec.describe(Defmastership::Modifier::UpdateDef) do
  subject(:modifier) { Defmastership::Modifier::ConcreteRef.new({}) }

  describe '.new' do
    it { expect(described_class.ancestors).to(include(Defmastership::Modifier::ModifierCommon)) }
    it { is_expected.not_to(be_nil) }
    it { is_expected.to(have_attributes(def_type: '')) }
  end

  describe '.replacement_methods' do
    it { expect(described_class.replacement_methods).to(eq(%i[replace_reference])) }
  end

  describe '#do_modifications' do
    subject(:modifier) { Defmastership::Modifier::ConcreteRef.new({ def_type: 'req' }) }

    let(:document)     { instance_double(Defmastership::Document, 'document')        }
    let(:definition)   { instance_double(Defmastership::Definition, 'definition')    }
    let(:definitions)  { { 'REFERENCE' => definition }                               }
    let(:adoc_sources) do
      {
        'file1.adoc' => "[define,req,REFERENCE]\nfile1 line2",
        'file2.adoc' => "file2 line1\nfile2 line2"
      }
    end
    let(:new_adoc_sources) { nil }

    before do
      allow(Defmastership::Document).to(receive(:new).and_return(document))
      allow(document).to(receive(:parse_file_with_preprocessor).with('file1.adoc'))
      allow(document).to(receive(:parse_file_with_preprocessor).with('file2.adoc'))

      modifier.do_modifications(adoc_sources)
    end

    it { expect(Defmastership::Document).to(have_received(:new)) }

    it do
      expect(document).to(
        have_received(:parse_file_with_preprocessor)
          .once.with('file1.adoc')
          .once.with('file2.adoc')
      )
    end

    it do
      expect(modifier.do_modifications(adoc_sources).fetch('file1.adoc'))
        .to(include('REFERENCE_something_'))
    end
  end

  describe '#replace_reference' do
    subject(:modifier) do
      Defmastership::Modifier::ConcreteRef.new(
        def_type: 'requirement'
      )
    end

    let(:definition)  { instance_double(Defmastership::Definition, 'definition') }
    let(:document)    { instance_double(Defmastership::Document, 'document')     }
    let(:definitions) { { 'REFERENCE' => definition }                            }

    before do
      allow(File).to(receive(:rename))
    end

    context 'when line do not match' do
      it do
        expect(modifier.replace_reference('[define---,req,REFERENCE(abcd)]'))
          .to(eq('[define---,req,REFERENCE(abcd)]'))
      end
    end

    context 'when definition has not the good type' do
      it do
        expect(modifier.replace_reference('[define,req,REFERENCE(abcd)]'))
          .to(eq('[define,req,REFERENCE(abcd)]'))
      end
    end

    context 'when definition has the good type' do
      before do
        allow(Defmastership::Document).to(receive(:new).and_return(document))
        allow(document).to(receive(:ref_to_def).with('REFERENCE').and_return(definition))
        allow(definition).to(receive(:sha256_short).and_return('~abcd1234'))
      end

      it do
        expect(modifier.replace_reference('[define,requirement,REFERENCE(abcd)]'))
          .to(eq('[define,requirement,REFERENCE_something_abcd]'))
      end

      it do
        expect(modifier.replace_reference('[define,requirement,REFERENCE(abcd~bad)]'))
          .to(eq('[define,requirement,REFERENCE_something_abcd]'))
      end

      it do
        expect(modifier.replace_reference('// [define,requirement,REFERENCE(abcd~bad)]'))
          .to(eq('// [define,requirement,REFERENCE(abcd~bad)]'))
      end
    end
  end
end
