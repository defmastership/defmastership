# Copyright (c) 2023 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/modifier/modifier_common')

# just a class for test
class DummyClassParent
  # This method smells of :reek:ControlParameter
  # This method smells of :reek:UtilityFunction
  def respond_to_missing?(method_name, *_)
    return true if method_name == :ploup

    false
  end
end

# just a class for test
class DummyClass < DummyClassParent
  include Defmastership::Modifier::ModifierCommon

  def self.replacement_methods
    %i[replace_pouet_by_foo replace_foo_by_zoo]
  end

  def self.default_config
    { something: :default }
  end

  def initialize(config)
    setup_modifier_module(config)
    super()
  end

  # This method smells of :reek:UtilityFunction
  def replace_pouet_by_foo(line)
    line.gsub('pouet', 'foo')
  end

  # This method smells of :reek:UtilityFunction
  def replace_foo_by_zoo(line)
    line.gsub('foo', 'zoo')
  end
end

RSpec.describe(Defmastership::Modifier::ModifierCommon) do
  subject(:modifier) { DummyClass.new({}) }

  describe '.new' do
    it { is_expected.not_to(be_nil) }
    it { is_expected.to(have_attributes(config: { something: :default })) }
    it { is_expected.to(have_attributes(changes: [])) }
  end

  context 'when want to use config smoothly' do
    subject(:modifier) { DummyClass.new(plop: 'Whatever', something: :not_default) }

    it { is_expected.to(have_attributes(config: { plop: 'Whatever', something: :not_default })) }
    it { is_expected.to(have_attributes(plop: 'Whatever')) }
    it { is_expected.not_to(respond_to(:pleeep)) }
    it { is_expected.to(respond_to(:ploup)) }

    it do
      expect { modifier.plooooop }
        .to(raise_error(NoMethodError))
    end
  end

  describe '#do_modifications' do
    it do
      adoc_sources = { first: "pouet\ntoto\npouet", second: "toto\npouet\ntoto" }

      expect(modifier.do_modifications(adoc_sources))
        .to(eq({ first: "zoo\ntoto\nzoo", second: "toto\nzoo\ntoto" }))
    end
  end
end
