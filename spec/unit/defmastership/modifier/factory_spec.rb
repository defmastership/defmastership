# Copyright (c) 2023 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/modifier/factory')

module Defmastership
  module Modifier
    # Modifier example
    class Toto
      include ModifierCommon
    end

    # Modifier example
    class TuTu
      include ModifierCommon
    end
  end
end

RSpec.describe(Defmastership::Modifier::Factory) do
  describe('.from_config') do
    let(:toto)        { instance_double(Defmastership::Modifier::Toto, 'toto') }
    let(:toto_config) { { type: 'toto', config: { p: 'whatever' } }            }
    let(:tutu)        { instance_double(Defmastership::Modifier::TuTu, 'tutu') }
    let(:tutu_config) { { type: 'tu_tu', config: { p: 'whatever' } }           }

    before do
      allow(Defmastership::Modifier::Toto).to(receive(:new).with(toto_config[:config]).and_return(toto))
      allow(Defmastership::Modifier::TuTu).to(receive(:new).with(tutu_config[:config]).and_return(tutu))
    end

    it { expect(described_class.from_config(toto_config)).to(be(toto)) }
    it { expect(described_class.from_config(tutu_config)).to(be(tutu)) }

    it do
      described_class.from_config(toto_config)
      expect(Defmastership::Modifier::Toto).to(have_received(:new).with(toto_config[:config]))
    end

    it do
      described_class.from_config(tutu_config)
      expect(Defmastership::Modifier::TuTu).to(have_received(:new).with(tutu_config[:config]))
    end
  end
end
