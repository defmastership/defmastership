# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/modifier/update_def_checksum')

RSpec.describe(Defmastership::Modifier::UpdateDefChecksum) do
  subject(:modifier) { described_class.new({}) }

  describe '.new' do
    it { expect(described_class.ancestors).to(include(Defmastership::Modifier::ModifierCommon)) }
    it { is_expected.not_to(be_nil) }
    it { is_expected.to(have_attributes(def_type: '')) }
  end

  describe '.replacement_methods' do
    it { expect(described_class.replacement_methods).to(eq(%i[replace_reference])) }
  end

  describe '#do_modifications' do
    subject(:modifier) { described_class.new({ def_type: 'req' })                 }

    let(:document)     { instance_double(Defmastership::Document, 'document')     }
    let(:definition)   { instance_double(Defmastership::Definition, 'definition') }
    let(:definitions)  { { 'REFERENCE' => definition }                            }
    let(:adoc_sources) do
      {
        'file1.adoc' => "[define,req,REFERENCE]\nfile1 line2",
        'file2.adoc' => "file2 line1\nfile2 line2"
      }
    end
    let(:new_adoc_sources) { nil }

    before do
      allow(Defmastership::Document).to(receive(:new).and_return(document))
      allow(document).to(receive(:parse_file_with_preprocessor))
      allow(document).to(receive(:ref_to_def).with('REFERENCE').and_return(definition))
      allow(definition).to(receive(:sha256_short).and_return('~abcd1234'))

      modifier.do_modifications(adoc_sources)
    end

    it { expect(Defmastership::Document).to(have_received(:new)) }

    it do
      expect(document).to(
        have_received(:parse_file_with_preprocessor)
          .once.with('file1.adoc')
          .once.with('file2.adoc')
      )
    end

    it do
      expect(modifier.do_modifications(adoc_sources).fetch('file1.adoc')).to(include('abcd1234'))
    end
  end

  describe '#replace' do
    subject(:modifier) do
      described_class.new(
        def_type: 'requirement'
      )
    end

    let(:definition)  { instance_double(Defmastership::Definition, 'definition') }
    let(:document)    { instance_double(Defmastership::Document, 'document')     }
    let(:definitions) { { 'REFERENCE' => definition }                            }

    before do
      allow(File).to(receive(:rename))
    end

    context 'when definition has not the good type' do
      it do
        expect(modifier.replace_reference('[define,req,REFERENCE]'))
          .to(eq('[define,req,REFERENCE]'))
      end
    end

    context 'when definition has the good type' do
      before do
        allow(Defmastership::Document).to(receive(:new).and_return(document))
        allow(document).to(receive(:ref_to_def).with('REFERENCE').and_return(definition))
        allow(definition).to(receive(:sha256_short).and_return('~abcd1234'))
      end

      it do
        expect(modifier.replace_reference('[define,requirement,REFERENCE]'))
          .to(eq('[define,requirement,REFERENCE(~abcd1234)]'))
      end

      it do
        expect(modifier.replace_reference('[define,requirement,REFERENCE(~bad)]'))
          .to(eq('[define,requirement,REFERENCE(~abcd1234)]'))
      end

      it do
        expect(modifier.replace_reference('[define,requirement,REFERENCE(toto~bad)]'))
          .to(eq('[define,requirement,REFERENCE(toto~abcd1234)]'))
      end

      it do
        expect(modifier.replace_reference('[define,requirement,REFERENCE(toto)]'))
          .to(eq('[define,requirement,REFERENCE(toto~abcd1234)]'))
      end
    end
  end
end
