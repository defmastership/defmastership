# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

# Ensure we require the local version and not one we might have
# installed already
require(
  File.join(
    [
      File.dirname(__FILE__),
      'lib',
      'defmastership',
      'version.rb'
    ]
  )
)

Gem::Specification.new do |spec|
  spec.metadata = {
    'rubygems_mfa_required' => 'true'
  }
  spec.required_ruby_version = '>= 3.0'
  spec.name = 'defmastership'
  spec.version = Defmastership::VERSION
  spec.author = 'Jérôme Arbez-Gindre'
  spec.email = 'jeromearbezgindre@gmail.com'
  spec.licenses = ['MIT']
  spec.homepage = 'https://gitlab.com/jjag/defmastership/'
  spec.platform = Gem::Platform::RUBY
  spec.summary = 'Handling of references and definitions with asciidoctor'
  spec.files = `git ls-files`.split("\n")
  spec.require_paths << 'lib'
  spec.bindir = 'bin'
  spec.executables << 'defmastership'
  spec.add_dependency('aasm', '~> 5.5')
  spec.add_dependency('asciidoctor', '~> 2.0')
  spec.add_dependency('csv', '~> 3.3')
  spec.add_dependency('defmastership-core', '~> 1.5')
  spec.add_dependency('git', '~> 2.3')
  spec.add_dependency('gli', '~> 2.22')
  spec.add_dependency('ostruct', '~> 0.6')
end
