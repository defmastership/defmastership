# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/core/constants')

# Main module of the application
module Defmastership
  # Contains regexp / action couples
  Filter = Struct.new(:regexp, :event, :consumed_line)
  private_constant :Filter

  FILTERS = [
    Filter.new(Core::DMRegexp::VARIABLE_DEF,  :new_variable_def,     false),
    Filter.new(Core::DMRegexp::VARIABLE_USE,  :new_variable_use,     false),
    Filter.new(Core::DMRegexp::DEFINITION,    :new_definition,       true),
    Filter.new(Core::DMRegexp::EREF_CONFIG,   :new_eref_setup,       true),
    Filter.new(Core::DMRegexp::EREF_DEF,      :new_eref_def,         false),
    Filter.new(Core::DMRegexp::IREF_DEF,      :new_iref_def,         false),
    Filter.new(Core::DMRegexp::ATTR_CONFIG,   :new_attribute_conf,   true),
    Filter.new(Core::DMRegexp::ATTR_SET,      :new_attribute_value,  false),
    Filter.new(Core::DMRegexp::BLOCK,         :block_delimiter,      true),
    Filter.new(Core::DMRegexp::EMPTY_LINE,    :empty_line,           false),
    Filter.new(Core::DMRegexp::WHATEVER,      :new_line,             true)
  ].freeze
  private_constant :FILTERS
end
