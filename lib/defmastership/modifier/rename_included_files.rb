# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/core/constants')
require('defmastership/matching_line')
require('defmastership/modifier/modifier_common')

module Defmastership
  # defintion of the Rename Included Files Modifier
  module Modifier
    LOCAL_FILTERS = [
      Filter.new(Core::DMRegexp::VARIABLE_DEF,  :new_variable_def),
      Filter.new(Core::DMRegexp::DEFINITION,    :new_definition),
      Filter.new(Core::DMRegexp::BLOCK,         :block_delimiter),
      Filter.new(Core::DMRegexp::EMPTY_LINE,    :empty_line),
      Filter.new(Core::DMRegexp::WHATEVER,      :new_line)
    ].freeze
    private_constant :LOCAL_FILTERS

    # Change included filenames on one line at a time
    class RenameIncludedFiles
      include ModifierCommon

      PARSER_ACTIONS = {
        new_variable_def: lambda { |matching_line|
          @variables.merge!(Helper.variable_def_hash(matching_line.match))
        },
        add_line: proc { |_| },
        add_new_definition: lambda { |matching_line|
          config[:reference] = matching_line.match[:reference]
        }
      }.freeze

      private_constant :PARSER_ACTIONS

      # @return [Array<Symbol>] the only replacement method symbol
      def self.replacement_methods
        %i[replace]
      end

      # @return [Hash{Symbol => Object}] the default configuration
      def self.default_config
        {
          from_regexp: '',
          to_template: ''
        }
      end

      # @param config [YAML] the modifier's provided configurations
      def initialize(config)
        @variables = {}
        @definition_parser = DefinitionParser.new(self)

        setup_modifier_module(config)
      end

      # Allow to add methods from parser actions
      #
      # @param method_name [Symbol] the name of the method
      # @param args[Array<Object>] the arguments of the method
      def method_missing(method_name, *args)
        action = PARSER_ACTIONS[method_name]
        return instance_exec(*args, &action) if action

        super
      end

      # Allow to check if a parser action is available
      #
      # @param method_name [Symbol] the name of the method
      # @param args[Array<Object>] the arguments of the method
      def respond_to_missing?(method_name, *args)
        PARSER_ACTIONS.key?(method_name) || super
      end

      # Modify line if it match
      #
      # @return [String] the modified line
      def replace(line)
        match = matched?(line)

        return line unless match

        new_line = build_new_include_line(match, line)

        rename_file(line, new_line)

        new_line
      end

      private

      def build_new_include_line(match, line)
        line.sub(Helper.complete_regexp_from(from_regexp)) do
          Helper.text_with(match, to_template % Helper.hmerge(config, match))
        end
      end

      def matched?(line)
        return false unless concerned_line?(line)
        return false unless line =~ Helper.complete_regexp_from(from_regexp)

        match = Regexp.last_match

        return false if config.key?(:cancel_if_match) && match[:filename].match?(cancel_if_match)

        match
      end

      def concerned_line?(line)
        parse(line)

        return false if @definition_parser.idle?

        true
      end

      def parse(line)
        Helper.apply_filters_until_consumed(line) do |event, match|
          generate_event(event, MatchingLine.new(match))
        end
      end

      def rename_file(from, to)
        filename_from = Helper.extract_filename(from, @variables)
        filename_to = Helper.extract_filename(to, @variables)
        changes << [filename_from, filename_to]
        File.rename(filename_from, filename_to)
      end

      def generate_event(event, matching_line)
        if PARSER_ACTIONS.key?(event)
          public_send(event, matching_line)
        else
          @definition_parser.public_send(event, matching_line)
        end
      end

      # Helper functions
      module Helper
        # @param include_statement [String] the overall include statement
        # @param variables [Hash{Symbol => String}] asciidoc variables for replacement
        # @return [String] the path+filename from 'include' statement
        def self.extract_filename(include_statement, variables)
          filename = filename_from_include_statement(include_statement)

          filename_replace_all_variables(filename, variables).chomp
        end

        # @param include_statement [String] the overall include statement
        # @return [String] the path+filename from 'include' statement
        def self.filename_from_include_statement(include_statement)
          include_statement
            .sub(Regexp.new(Core::DMRegexp::INCLUDE_KEYWORD), '')
            .sub(Regexp.new(Core::DMRegexp::INCLUDE_OPTIONS), '')
        end

        # @param filename [String] the path+filename from 'include' statement
        # @param variables [Hash{Symbol => String}] asciidoc variables for replacement
        # @return [String] the path+filename from 'include' statement with replaced variables
        def self.filename_replace_all_variables(filename, variables)
          filename.scan(Core::DMRegexp::VARIABLE_USE) do
            varname = Regexp.last_match[:varname]
            value = variables.fetch(varname.to_sym)
            filename = filename_replace_one_variable(filename, varname, value)
          end
          filename
        end

        # @param filename [String] the path+filename from 'include' statement
        # @param varname [Symbol] asciidoc variable symbol
        # @param value [Symbol] asciidoc variable value
        # @return [String] the path+filename from 'include' statement with one replace variable
        def self.filename_replace_one_variable(filename, varname, value)
          filename.sub("{#{varname}}", value)
        end

        # @param from [String] the piece of regexp to match the filename
        # @return [Regexp] the regexp to match overal include statement
        def self.complete_regexp_from(from)
          Regexp.new(
            Core::DMRegexp::INCLUDE_KEYWORD + Core::DMRegexp::INCLUDE_PATH +
            "(?<filename>#{from})" + Core::DMRegexp::INCLUDE_OPTIONS
          )
        end

        # @param match [MatchData] from a Regexp match
        # @param to [String] the replacement text
        # @return [String] the include statement with replaced filenames
        def self.text_with(match, to)
          "include::#{match[:path]}#{to}[#{match[:options]}]"
        end

        # @param config [Hash{Symbol => Object}] the provided configuration
        # @param match [MatchData] the match from a regexp with named back refs
        # @return [Hash{Symbol => String}] config hash merged with match data
        def self.hmerge(config, match)
          config.merge(match.names.map(&:to_sym).zip(match.captures).to_h)
        end

        # @param match [MatchData] the match from a regexp with 'varname' named back refs
        # @return [Hash{Symbol => String}] a hash with only one key from 'varname'
        def self.variable_def_hash(match)
          { match[:varname].to_sym => match[:value] }
        end

        # Helper function: apply all filters
        #
        # @param line [String] the line
        def self.apply_filters_until_consumed(line)
          LOCAL_FILTERS.each do |filter|
            next unless line.match(filter.regexp)

            yield(filter.event, Regexp.last_match)
            break
          end
        end
      end
      private_constant :Helper
    end
  end
end
