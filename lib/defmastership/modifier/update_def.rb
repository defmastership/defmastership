# Copyright (c) 2024 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/core/constants')

module Defmastership
  module Modifier
    # @abstract Subclass and define +reference_replacement+ to implement a
    # custom reference replacement class
    class UpdateDef
      include ModifierCommon

      attr_reader :document

      # @return [Hash{Symbol => Object}] the default configuration
      def self.default_config
        {
          def_type: ''
        }
      end

      # @return [Array<Symbol>] the methods's symbols that will be called in sequence for modifications
      def self.replacement_methods
        %i[replace_reference]
      end

      # Builds a Regexp to match a particular reference defintion (with its
      # optional version and checksum)
      #
      # @param reference [String] the reference
      # @return [Regexp] the built Regexp
      def self.reference_regexp(reference)
        Regexp.new("#{reference}#{Core::DMRegexp::DEF_VERSION_AND_CHECKSUM}")
      end

      # @param config [YAML] the modifier's provided configurations
      def initialize(config)
        @document = Document.new

        setup_modifier_module(config)
      end

      # Apply the modifier on all provided asciidoc sources based on modifier's
      # +self.replacement_methods+ list
      #
      # @param adoc_sources [Hash{String => String}] asciidoc sources
      #  * :key filename
      #  * :value file content
      def do_modifications(adoc_sources)
        adoc_sources.each_key do |adoc_file|
          document.parse_file_with_preprocessor(adoc_file)
        end

        super
      end

      # Called on each line for an opportunity for text replacement
      #
      # @param line [String] line from asciidoc sources files
      # @return [String] the modified line
      def replace_reference(line)
        match = line.match(Core::DMRegexp::DEFINITION)

        return line unless match
        return line unless match[:type] == def_type

        reference = match[:reference]
        line.sub(self.class.reference_regexp(reference), reference_replacement(reference, match))
      end
    end
  end
end
