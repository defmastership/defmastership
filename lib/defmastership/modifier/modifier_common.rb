# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

module Defmastership
  module Modifier
    # Defines common methods and attributes for line modifiers
    #
    # @abstract Include and define +self.replacement_methods+ and +self.default_config+
    module ModifierCommon
      # Stores the configuration of this modifier
      # @return [YAML] configuration as eventualy modified
      attr_reader :config
      # Provides the list of performed modifications
      # @return [Array<Array<String>>] List of performed modifications
      # (each line: [Modifier, Was, Becomes])
      attr_reader :changes

      # Setup config from class's default config and provided config
      #
      # @param config [YAML] the modifier's provided configurations
      def setup_modifier_module(config)
        @config = self.class.default_config.merge(config)
        @changes = []
      end

      # Allow to view config items as methods
      #
      # @param method_name [Symbol] the name of the method
      # @param args[Array<Object>] the arguments of the method
      def method_missing(method_name, *args)
        config_method_name = config[method_name]
        return config_method_name if config_method_name

        super
      end

      # Allow to check if a config item is available
      #
      # @param method_name [Symbol] the name of the method
      # @param args[Array<Object>] the arguments of the method
      def respond_to_missing?(method_name, *args)
        config.key?(method_name) || super
      end

      # Apply the modifier on all provided asciidoc sources based on modifier's
      # +replacement_methods+ list
      #
      # @param adoc_sources [Hash{String => String}] asciidoc sources
      #  * :key filename
      #  * :value file content
      def do_modifications(adoc_sources)
        self.class.replacement_methods.reduce(adoc_sources) do |texts, method|
          apply_to_all(texts, method)
        end
      end

      private

      def apply_to_all(texts, method)
        texts.transform_values do |text|
          apply_to_one(text, method)
        end
      end

      def apply_to_one(text, method)
        text.lines.map { |line| public_send(method, line) }
            .join
      end
    end
  end
end
