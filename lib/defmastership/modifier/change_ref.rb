# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require 'defmastership/core/constants'
require('defmastership/core/parsing_state')
require 'defmastership/modifier/modifier_common'

module Defmastership
  module Modifier
    # Change references from temporary to definitive with multiple RefChangers
    class ChangeRef
      include ModifierCommon

      # [Regexp] match all text before the definition's reference
      DEF_BEFORE_REF = <<~"BEF".freeze
        ^
        \\s*
        \\[
        #{Core::DMRegexp::DEF_KEYWORD}
        #{Core::DMRegexp::DEF_TYPE}
        ,
        \\s*
      BEF
      private_constant :DEF_BEFORE_REF

      # [Regexp] match all text after the definition's reference
      DEF_AFTER_REF = <<~"AFT".freeze
        \\s*
        #{Core::DMRegexp::DEF_SUMMARY}
        #{Core::DMRegexp::DEF_LABELS}
        \\s*
        \\]
      AFT
      private_constant :DEF_AFTER_REF

      # [Regexp] match the begining of an internal cross reference
      IREF_DEF_BEF = 'defs:iref\[\s*'
      private_constant :IREF_DEF_BEF
      # [Regexp] match the end of an internal cross reference
      IREF_DEF_AFT = '\s*\]'
      private_constant :IREF_DEF_AFT

      REGEXP_FROM = {
        definition: {
          before: DEF_BEFORE_REF,
          after: DEF_AFTER_REF
        },
        iref: {
          before: IREF_DEF_BEF,
          after: IREF_DEF_AFT
        }
      }.freeze

      private_constant :REGEXP_FROM

      # Methods's symbols will be called in sequence to perform the document modifications
      #
      # @return [Array<Symbol>] the two symbols of replacement methods
      def self.replacement_methods
        %i[replace_refdef replace_irefs]
      end

      # @return [Hash{Symbol => Object}] the default configuration
      def self.default_config
        {
          from_regexp: '',
          to_template: '',
          next_ref: 0
        }
      end

      # @param config [YAML] the modifier's provided configurations
      def initialize(config)
        @parsing_state = Core::ParsingState.new

        setup_modifier_module(config)
      end

      # Replace the definition's ref in the line if relevant
      #
      # @param line [String] the current line
      # @return [String] the modified line
      def replace_refdef(line)
        if @parsing_state.enabled?(line)
          do_replace_refdef(line)
        else
          line
        end
      end

      # Replace the definition's refs in intenal refs
      #
      # @param line [String] the current line
      # @return [String] the modified line
      def replace_irefs(line)
        changes.reduce(line) do |res_line, (from, to)|
          res_line.gsub(Helper.regexp_from(:iref, from)) do
            Helper.text_with(Regexp.last_match, to)
          end
        end
      end

      private

      def do_replace_refdef(line)
        line.sub(Helper.regexp_from(:definition, from_regexp)) do
          replacement_from(Regexp.last_match)
        end
      end

      def replacement_from(match)
        replacement = to_template % hmerge(match)
        changes << [match[:from], replacement]
        config[:next_ref] += 1
        Helper.text_with(match, replacement)
      end

      def hmerge(match)
        config.merge(match.named_captures.transform_keys(&:to_sym))
      end

      # Helper functions
      module Helper
        # @param  const [Symbol] the key to retrieve preamble and post-amble
        # @param  from [Symbol] the piece of text to be replaced
        # @return [Regexp] a built regexp
        def self.regexp_from(const, from)
          regexps = REGEXP_FROM.fetch(const)
          regexp_str =
            "(?<before>#{regexps[:before]})" \
            "(?<from>#{from})" \
            "#{Core::DMRegexp::DEF_VERSION_AND_CHECKSUM}" \
            "(?<after>#{regexps[:after]})"
          Regexp.new(regexp_str, Regexp::EXTENDED)
        end

        # @param  match [MatchData] The match form Regepxp match
        # @param  replacement [String] the reference replacement text
        # @return [String] the overall replaced text
        def self.text_with(match, replacement)
          match[:before] + replacement + (match[:version_and_checksum] || '') + match[:after]
        end
      end
      private_constant :Helper
    end
  end
end
