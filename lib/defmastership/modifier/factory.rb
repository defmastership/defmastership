# Copyright (c) 2023 Jerome Arbez-Gindre
# frozen_string_literal: true

module Defmastership
  module Modifier
    # build modifiers from a piece of configuration
    module Factory
      # Build a concrete class from config 'type' field
      #
      # @param config [YAML] piece of configuration for this Modifier
      def self.from_config(config)
        class_name = config.fetch(:type).split('_').map(&:capitalize).join
        Modifier.const_get(class_name).new(config.fetch(:config))
      end
    end
  end
end
