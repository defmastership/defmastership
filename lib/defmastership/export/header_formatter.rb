# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('csv')

module Defmastership
  module Export
    # format CSV header (first line) for one document
    class HeaderFormatter
      # @param doc [Document] the document to export
      def initialize(doc)
        @doc = doc
      end

      %i[type reference summary value checksum].each do |method_symbol|
        define_method(method_symbol) do
          [method_symbol.to_s.capitalize]
        end
      end

      # @return [Array<String>] Optional wrong_explicit_checksum column caption
      def wrong_explicit_checksum
        @doc.wrong_explicit_checksum? ? ['Wrong explicit checksum'] : []
      end

      # @return [Array<String>] Optional version column captions
      def explicit_version
        @doc.explicit_version? ? ['Version'] : []
      end

      # @return [Array<String>] Optional labels columns captions
      def labels
        @doc.labels.empty? ? [] : %w[Labels]
      end

      # @return [Array<String>] Optional external refs columns captions
      def eref
        @doc.eref.map { |_, ref| ref.fetch(:prefix) }
      end

      # @return [Array<String>] Optional internal refs column caption
      def iref
        @doc.iref ? ['Internal links'] : []
      end

      # @return [Array<String>] Optional attributes columns captions
      def attributes
        @doc.attributes.map { |_, value| value }
      end
    end
  end
end
