# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

module Defmastership
  module Export
    # format CSV lines per definition
    class BodyFormatter
      # @param doc [Document] the document to export
      # @param definition [String] the definition to export
      def initialize(doc, definition)
        @doc = doc
        @definition = definition
      end

      %i[type reference summary value].each do |method_symbol|
        define_method(method_symbol) do
          [@definition.public_send(method_symbol)]
        end
      end

      # @return [Array<String>] checksum column value
      def checksum
        [@definition.sha256_short]
      end

      # @return [Array<String>] Optional wrong_explicit_checksum column value
      def wrong_explicit_checksum
        wrong_explicit_checksum = @definition.wrong_explicit_checksum
        wrong_explicit_checksum ? [wrong_explicit_checksum] : ['']
      end

      # @return [Array<String>] Optional version column value
      def explicit_version
        explicit_version = @definition.explicit_version
        explicit_version ? [explicit_version] : ['']
      end

      # @return [Array<String>] Optional labels columns values
      def labels
        [@definition.labels.join("\n")]
      end

      # @return [Array<String>] Optional external refs columns values
      def eref
        @doc.eref.map { |key,| @definition.eref.fetch(key, []).join("\n") }
      end

      # @return [Array<String>] Optional internal refs column value
      def iref
        [@definition.iref.join("\n")]
      end

      # @return [Array<String>] Optional attributes columns values
      def attributes
        @doc.attributes.map { |key,| @definition.attributes.fetch(key, '') }
      end
    end
  end
end
