# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

module Defmastership
  # [String] Gem version
  VERSION = '1.3.0'
  public_constant :VERSION
end
