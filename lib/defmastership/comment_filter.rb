# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

# adding a method to string to figure out if a string is commented or
# not.
class String
  # @return [Boolean] true if the line is commented
  def commented?
    !!match(Defmastership::Core::DMRegexp::SINGLE_LINE_COMMENT)
  end
end
