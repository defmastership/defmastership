# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

module Defmastership
  # a composite class storing a line and the corresponding match
  MatchingLine =
    Struct.new(:match, :line) do
      # Act as a Hasg and delegate to the Match
      def [](key)
        value = match[key]
        return value if value

        super
      end
    end

  public_constant :MatchingLine
end
