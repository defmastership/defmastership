# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('asciidoctor')
require('defmastership/core/constants')
require('defmastership/core/parsing_state')
require('defmastership/matching_line')

# Contains the content of a Defmastership document: mainly definitions
module Defmastership
  # Class to host data of Document class
  DocumentData = Struct.new(
    :definitions,
    :labels,
    :eref,
    :iref,
    :attributes,
    :variables
  )

  private_constant :DocumentData

  PARSER_ACTIONS = {
    add_new_definition: lambda { |matching_line|
      # rebuild the match to take into account eventual variable substitutions
      match = matching_line.line.match(Core::DMRegexp::DEFINITION)
      definition = Definition.new(match)
      labels.merge(definition.labels)
      definitions << definition
    },
    add_line: lambda { |matching_line|
      line = matching_line.line
      definitions.last << line
    },
    new_eref_setup: lambda { |matching_line|
      reference = matching_line[:reference].to_sym
      eref[reference] ||= {}
      eref[reference][matching_line[:symb].to_sym] = matching_line[:value]
    },
    new_eref_def: lambda { |matching_line|
      definitions.last.add_eref(matching_line[:reference].to_sym, matching_line[:extrefs])
    },
    new_iref_def: lambda { |matching_line|
      self.has_iref = true
      line = matching_line.line
      line.scan(Core::DMRegexp::IREF_DEF) do |_|
        definitions.last.add_iref(Regexp.last_match[:intref])
      end
    },
    new_attribute_conf: lambda { |matching_line|
      attributes[matching_line[:attr].to_sym] = matching_line[:prefix]
    },
    new_attribute_value: lambda { |matching_line|
      definitions.last.set_attribute(matching_line[:attr].to_sym, matching_line[:value])
    },
    new_variable_def: lambda { |matching_line|
      variables[matching_line[:varname].to_sym] = matching_line[:value]
    },
    new_variable_use: lambda { |matching_line|
      line = matching_line.line
      line.scan(Core::DMRegexp::VARIABLE_USE) do |_|
        varname = Regexp.last_match[:varname]
        value = variables[varname.to_sym]

        next unless value

        line = line.gsub("{#{varname}}", value)
      end
      line
    }
  }.freeze

  private_constant :PARSER_ACTIONS

  # Reflects document structure from a definition point of view
  class Document
    extend Forwardable
    def_delegators :@data, :definitions, :labels, :eref, :iref, :attributes, :variables

    def initialize
      @data = DocumentData.new([], Set.new, {}, false, {}, {})
      @parsing_state = Core::ParsingState.new
      @definition_parser = DefinitionParser.new(self)
    end

    # Parse asciidoc file after [Asciidoctor] preprocessing
    #
    # @param adoc_file [String] the file to parse
    def parse_file_with_preprocessor(adoc_file)
      do_parse(Asciidoctor.load_file(adoc_file, safe: :unsafe, parse: false).reader.read_lines)
    end

    # @return [Boolean] true if the document has definitions with summary
    def summaries?
      definitions.reduce(false) do |res, definition|
        res || !!definition.summary
      end
    end

    # @return [Boolean] true if the document has definitions with wrong explicit checksum
    def wrong_explicit_checksum?
      definitions.reduce(false) do |res, definition|
        res || !!definition.wrong_explicit_checksum
      end
    end

    # @return [Boolean] true if the document has definitions with explicit versions
    def explicit_version?
      definitions.reduce(false) do |res, definition|
        res || !!definition.explicit_version
      end
    end

    # Allow to add methods from parser actions
    #
    # @param method_name [Symbol] the name of the method
    # @param args[Array<Object>] the arguments of the method
    def method_missing(method_name, *args)
      action = PARSER_ACTIONS[method_name]
      return instance_exec(*args, &action) if action

      super
    end

    # Allow to check if a parser action is available
    #
    # @param method_name [Symbol] the name of the method
    # @param _args[Array<Object>] the arguments of the method
    # This method smells of :reek:UtilityFunction
    def respond_to_missing?(method_name, *_args)
      PARSER_ACTIONS.key?(method_name)
    end

    # @param reference [String] the  defintion from the reference
    # @return [Definition] the defintion from the reference
    def ref_to_def(reference)
      definitions.find { |definition| definition.reference == reference }
    end

    private

    def has_iref=(value)
      @data.iref = value
    end

    def do_parse(lines)
      lines.each do |line|
        if @parsing_state.enabled?(line)
          apply_filters(line)
        else
          generate_event(:new_line, MatchingLine.new(nil, line))
        end
      end
    end

    def apply_filters(line)
      Helper.reduce_filters_until_consumed(line) do |event, match, updated_line|
        generate_event(event, MatchingLine.new(match, updated_line))
      end
    end

    def generate_event(event, matching_line)
      if PARSER_ACTIONS.key?(event)
        public_send(event, matching_line)
      else
        @definition_parser.public_send(event, matching_line)
      end
    end

    # Helper functions
    module Helper
      # Helper function: apply all filters
      #
      # @param line [String] the line
      def self.reduce_filters_until_consumed(line)
        FILTERS.reduce(line) do |res, filter|
          next res unless line.match(filter.regexp)

          res = yield(filter.event, Regexp.last_match, res)
          break if filter.consumed_line

          res
        end
      end
    end
    private_constant :Helper
  end
end
