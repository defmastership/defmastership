# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('aasm')

module Defmastership
  # Defmastership definition: contains all data of a definition
  class DefinitionParser
    include AASM

    def initialize(document)
      @document = document
    end

    aasm do
      state :idle, initial: true
      state :wait_content
      state :in_block
      state :single_para

      event :new_definition do
        transitions from: :idle, to: :wait_content, after: ->(*args) { @document.add_new_definition(*args) }
      end

      event :block_delimiter do
        transitions from: :wait_content,                 to: :in_block
        transitions from: %i[in_block idle single_para], to: :idle
      end

      event :new_line do
        transitions from: %i[wait_content single_para],
                    to: :single_para,
                    after: ->(*args) { @document.add_line(*args) }
        transitions from: :in_block,
                    to: :in_block,
                    after: ->(*args) { @document.add_line(*args) }
        transitions from: :idle, to: :idle
      end

      event :empty_line do
        transitions from: %i[wait_content single_para idle], to: :idle
        transitions from: :in_block, to: :in_block
      end
    end
  end
end
