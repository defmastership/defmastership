# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

require('defmastership/version')

# Add requires for other files you add to your project here, so
# you just need to require this one file in your bin file
require('defmastership/batch_modifier')
require('defmastership/comment_filter')
require('defmastership/definition')
require('defmastership/definition_parser')
require('defmastership/document')
require('defmastership/export/csv/formatter')
require('defmastership/filters')
require('defmastership/modifier/change_ref')
require('defmastership/modifier/factory')
require('defmastership/modifier/rename_included_files')
require('defmastership/modifier/update_def_checksum')
require('defmastership/modifier/update_def_version')
