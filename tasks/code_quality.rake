# Copyright (c) 2023 Jerome Arbez-Gindre
# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
namespace 'quality' do
  begin
    require('rubocop/rake_task')

    RuboCop::RakeTask.new do |task|
      task.options << '--display-cop-names'
      task.options << '--config=config/rubocop.yml'
    end
  rescue LoadError
    task(:rubocop) do
      puts('Install rubocop to run its rake tasks')
    end
  end

  begin
    require('reek/rake/task')

    Reek::Rake::Task.new do |task|
      task.fail_on_error = true
      task.source_files = '{lib,spec}/**/*.rb'
      task.reek_opts = '--config config/reek.yml --single-line'
    end
  rescue LoadError
    task(:reek) do
      puts('Install reek to run its rake tasks')
    end
  end

  begin
    require('flay_task')

    FlayTask.new(:flay, 200, %w[bin lib]) do |task|
      task.verbose = true
    end
  rescue LoadError
    task(:flay) do
      puts('Install flay to run its rake tasks')
    end
  end

  begin
    require('rubycritic/rake_task')

    RubyCritic::RakeTask.new do |task|
      task.verbose = true
      task.options = '--no-browser'
    end
  rescue LoadError
    task(:rubycritic) do
      puts('Install rubycritic to run its rake tasks')
    end
  end

  desc 'Runs all quality code check'
  task(
    all: [
      'quality:rubocop',
      'quality:reek',
      'quality:flay',
      'quality:rubycritic'
    ]
  )
end
# rubocop:enable Metrics/BlockLength

desc 'Synonym for quality:rubocop'
task(rubocop: 'quality:rubocop')

desc 'Synonym for quality:reek'
task(reek: 'quality:reek')
