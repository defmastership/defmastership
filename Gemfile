# Copyright (c) 2020 Jerome Arbez-Gindre
# frozen_string_literal: true

source 'https://rubygems.org'

gemspec

ruby RUBY_VERSION

# rubocop:disable Metrics/BlockLength
group :development do
  # cucumber steps for command line tests
  gem 'aruba',                 '~> 2.3'
  # bdd
  gem 'cucumber',              '~> 9.2'
  # code duplication
  gem 'flay',                  '~> 2.13'
  # automatic test run
  gem 'guard',                 '~> 2.19'
  # automatic update invocation
  gem 'guard-bundler',         '~> 3.0'
  # automatic style check
  gem 'guard-reek',            '~> 1.2'
  # automatic tdd
  gem 'guard-rspec',           '~> 4.7'
  # automatic style check
  gem 'guard-rubocop',         '~> 1.5'
  # mutation testing
  plan = 'oss'
  key = '7oac4dMz95cTUuFPtGDfTDSQep6ZhdGW'
  source "https://#{plan}:#{key}@gem.mutant.dev" do
    # license needed
    gem 'mutant-license', '~> 0.1'
  end
  # mutation testing
  gem 'mutant-rspec',          '~> 0.12'
  # to parse and execute Rakefile
  gem 'rake',                  '~> 13.2'
  # needed by yard to render documentation
  gem 'rdoc',                  '~> 6.12'
  # tdd
  gem 'rspec',                 '~> 3.13'
  # code needs to be clean
  gem 'rubocop',               '~> 1.72'
  # code needs to be clean
  gem 'rubocop-performance',   '~> 1.24'
  # test code needs to be clean
  gem 'rubocop-rspec',         '~> 3.4'
  # Rakefiles need to be clean
  gem 'rubocop-rake',          '~> 0.6'
  # detect selling code
  gem 'reek',                  '~> 6.4'
  # my code needs to be critiqued
  gem 'rubycritic',            '~> 4.9'
  # Doc need to be clean
  gem 'rubocop-yard',          '~> 0.10'
  # What is tdd without code coverage ?
  gem 'simplecov',             '~> 0.22'
  # to document code
  gem 'yard',                  '~> 0.9'
end
# rubocop:enable Metrics/BlockLength

group :debugging do
  # Sometimes, we need to debug
  gem 'pry',                   '~> 0.15'
end
