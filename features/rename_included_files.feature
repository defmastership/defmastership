
Feature: The rename_included_files command
  As a Responsible of definitions for a given document.
  In order to easy traceability with included files,
  I want defmastership to change filename of included files to reflect
  reference.

  Scenario: do NOT change the filename of a file NOT included in a definition
    Given a file named "modifications.yml" with:
    """
    ---
    :rename_included_png:
      :type: rename_included_files
      :config:
        :from_regexp: (?<origin>.*\.png)
        :to_template: "%<reference>s_%<origin>s"
    """
    And a file named "thedoc.adoc" with:
    """
    include::any_path/one_file.png[]
    """
    And a directory named "any_path"
    And an empty file named "any_path/one_file.png"
    When I successfully run `defmastership modify --modifications rename_included_png thedoc.adoc`
    Then the stdout should not contain anything
    And the file "thedoc.adoc" should contain:
    """
    include::any_path/one_file.png[]
    """
    And the file "any_path/one_file.png" should exist

  Scenario: change the filename of a file included in a definition
    Given a file named "modifications.yml" with:
    """
    ---
    :rename_included_png:
      :type: rename_included_files
      :config:
        :from_regexp: (?<origin>.*\.png)
        :to_template: "%<reference>s_%<origin>s"
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-WHATEVER-123]
    include::any_path/one_file.png[]
    """
    And a directory named "any_path"
    And an empty file named "any_path/one_file.png"
    When I successfully run `defmastership modify --modifications rename_included_png thedoc.adoc`
    Then the stdout should not contain anything
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-WHATEVER-123]
    include::any_path/TOTO-WHATEVER-123_one_file.png[]
    """
    And the file "any_path/one_file.png" should not exist
    And the file "any_path/TOTO-WHATEVER-123_one_file.png" should exist

  Scenario: change the filename with options in include macro
    Given a file named "modifications.yml" with:
    """
    ---
    :rename_included_png:
      :type: rename_included_files
      :config:
        :from_regexp: (?<origin>.*\.png)
        :to_template: "%<reference>s_%<origin>s"
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-WHATEVER-123]
    include::any_path/one_file.png[leveloffset=offset,lines=ranges]
    """
    And a directory named "any_path"
    And an empty file named "any_path/one_file.png"
    When I successfully run `defmastership modify --modifications rename_included_png thedoc.adoc`
    Then the stdout should not contain anything
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-WHATEVER-123]
    include::any_path/TOTO-WHATEVER-123_one_file.png[leveloffset=offset,lines=ranges]
    """
    And the file "any_path/one_file.png" should not exist
    And the file "any_path/TOTO-WHATEVER-123_one_file.png" should exist

  Scenario: change the filename with variable in path
    Given a file named "modifications.yml" with:
    """
    ---
    :rename_included_png:
      :type: rename_included_files
      :config:
        :from_regexp: (?<origin>.*\.png)
        :to_template: "%<reference>s_%<origin>s"
    """
    And a file named "thedoc.adoc" with:
    """
    :any: one
    :path: two
    [define, requirement, TOTO-WHATEVER-123]
    --
    include::{any}_{path}/one_file.png[]
    --
    """
    And a directory named "one_two"
    And an empty file named "one_two/one_file.png"
    When I successfully run `defmastership modify --modifications rename_included_png thedoc.adoc`
    Then the stdout should not contain anything
    And the file "thedoc.adoc" should contain:
    """
    :any: one
    :path: two
    [define, requirement, TOTO-WHATEVER-123]
    --
    include::{any}_{path}/TOTO-WHATEVER-123_one_file.png[]
    --
    """
    And the file "one_two/one_file.png" should not exist
    And the file "one_two/TOTO-WHATEVER-123_one_file.png" should exist

  Scenario: DO NOT change the filename again and again
    Given a file named "modifications.yml" with:
    """
    ---
    :rename_included_png:
      :type: rename_included_files
      :config:
        :from_regexp: (?<origin>.*\.png)
        :cancel_if_match: TOTO-WHATEVER
        :to_template: "%<reference>s_%<origin>s"
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-WHATEVER-123]
    include::any_path/TOTO-WHATEVER-123_one_file.png[]
    """
    And a directory named "any_path"
    And an empty file named "any_path/TOTO-WHATEVER-123_one_file.png"
    When I successfully run `defmastership modify --modifications rename_included_png thedoc.adoc`
    Then the stdout should not contain anything
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-WHATEVER-123]
    include::any_path/TOTO-WHATEVER-123_one_file.png[]
    """
    And the file "any_path/one_file.png" should not exist
    And the file "any_path/TOTO-WHATEVER-123_one_file.png" should exist

