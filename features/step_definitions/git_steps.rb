# Copyright (c) 2024 Jerome Arbez-Gindre
# frozen_string_literal: true

require 'git'

Given('I initialize a git repo') do
  Git.init(Aruba.config.home_directory)
end

Given('I add and commit the {string} file') do |filename|
  git = Git.open(Aruba.config.home_directory)
  git.add(filename)
  git.commit('whatever')
end

Given('I set the {string} tag') do |tag_name|
  git = Git.open(Aruba.config.home_directory)
  git.add_tag(tag_name)
end
