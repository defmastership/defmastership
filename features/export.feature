Feature: The extract command
  As a Responsible of definitions
  In order to make statistics on definitions
  I want defmastership to extract definitions from asciidoctor documents

  Scenario: Extract one definition to CSV 
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line.",~b86dcbde
    """
    And the stdout should not contain anything

  Scenario: Extract to CSV with alternative CSV separator
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    When I successfully run `defmastership export --separator=';' toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type;Reference;Value;Checksum
    requirement;TOTO-0001;"Exemple of multiline requirement.
    Second line.";~b86dcbde
    """

  Scenario: Extract one definition with variable to CSV 
    Given a file named "toto.adoc" with:
    """
    :my_variable-with-dash: Variable content
    :variable2: Toto
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement with variable: {my_variable-with-dash}.
    {my_variable-with-dash} : {my_variable-with-dash} {variable2}.
    Third line with {not_defined_variable}.
    --
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum
    requirement,TOTO-0001,"Exemple of multiline requirement with variable: Variable content.
    Variable content : Variable content Toto.
    Third line with {not_defined_variable}.",~3bf370ed
    """
    And the stdout should not contain anything


  Scenario: Extract some definitions to CSV 
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    [define, something_else, TOTO-0003]
    only one paragraphe.
    with two sentences.
    --
    ignored multiline.
    Second line.
    --
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line.",~b86dcbde
    something_else,TOTO-0003,"only one paragraphe.
    with two sentences.",~4761e172
    """
    And the file "toto.csv" should not contain:
    """
    ignored multiline
    """
    And the stdout should not contain anything

  Scenario: Ignore definitions in literal block
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    ....
    [define, something_else, TOTO-0003]
    only one line
    ....
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should not contain:
    """
    something_else
    """
    And the stdout should not contain anything

  Scenario: Ignore definitions in example block
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    ----
    [define, something_else, TOTO-0003]
    only one line
    ----
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should not contain:
    """
    something_else
    """
    And the stdout should not contain anything

  Scenario: Ignore definitions in commment block
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    ////
    [define, something_else, TOTO-0003]
    only one line
    ////
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should not contain:
    """
    something_else
    """
    And the stdout should not contain anything

  Scenario: Handle mixes comment and literal blocks 
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    ////
    [define, whatever, TOTO-0003]
    ----
    only one line
    ////
    [define, something_else, TOTO-0003]
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    something_else
    """
    And the stdout should not contain anything

  Scenario: Extract definition with summary
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001, A nice summary]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Summary,Value,Checksum
    requirement,TOTO-0001,A nice summary,"Exemple of multiline requirement.
    Second line.",~f8092f4e
    """
    And the stdout should not contain anything

  Scenario: Extract definition with summary with variable
    Given a file named "toto.adoc" with:
    """
    :a_variable: summary
    [define, requirement, TOTO-0001, A nice {a_variable}]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Summary,Value,Checksum
    requirement,TOTO-0001,A nice summary,"Exemple of multiline requirement.
    Second line.",~f8092f4e
    """
    And the stdout should not contain anything

  Scenario: Extract definition with summary including comma
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001, " A nice, summary " ]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Summary,Value,Checksum
    requirement,TOTO-0001,"A nice, summary","Exemple of multiline requirement.
    Second line.",~77a203d4
    """
    And the stdout should not contain anything

  Scenario: Extract definition with labels 
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001, [label1, label2]]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum,Labels
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line.",~b86dcbde,"label1
    label2"
    """
    And the stdout should not contain anything

  Scenario: Extract one definition with external ref to CSV
    Given a file named "toto.adoc" with:
    """
    :eref-implements-prefix: Participate to:
    :eref-implements-url:    ./the_other_document.html
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    defs:eref[implements, [SYSTEM-0012, SYSTEM-0014]]
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum,Participate to:
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line.",~b86dcbde,"SYSTEM-0012
    SYSTEM-0014"
    """
    And the stdout should not contain anything

  Scenario: Extract one definition with external ref (with version and checksum) to CSV
    Given a file named "toto.adoc" with:
    """
    :eref-implements-prefix: Participate to:
    :eref-implements-url:    ./the_other_document.html
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    defs:eref[implements, [SYSTEM-0012(a), SYSTEM-0014, SYSTEM-0015(b~1223abcd)]]
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum,Participate to:
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line.",~b86dcbde,"SYSTEM-0012(a)
    SYSTEM-0014
    SYSTEM-0015(b~1223abcd)"
    """
    And the stdout should not contain anything

  Scenario: Extract one definition with missing external ref to CSV
    Given a file named "toto.adoc" with:
    """
    :eref-implements-prefix: Participate to:
    :eref-implements-url:    ./the_other_document.html
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum,Participate to:
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line.",~b86dcbde,
    """
    And the stdout should not contain anything

  Scenario: Extract one definition with external ref without url to CSV 
    Given a file named "toto.adoc" with:
    """
    :eref-implements-prefix: Participate to:
    :eref-implements-url:    none
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    defs:eref[implements, [SYSTEM-0012, SYSTEM-0014]]
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum,Participate to:
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line.",~b86dcbde,"SYSTEM-0012
    SYSTEM-0014"
    """
    And the stdout should not contain anything

  Scenario: Extract one definition with external ref to CSV without URL
    Given a file named "toto.adoc" with:
    """
    :eref-implements-prefix: Participate to:
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line.
    --
    defs:eref[implements, [SYSTEM-0012, SYSTEM-0014]]
    """
    When I successfully run `defmastership export toto.adoc`
    Then the stdout should not contain anything
    And the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum,Participate to:
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line.",~b86dcbde,"SYSTEM-0012
    SYSTEM-0014"
    """

  Scenario: Extract one definition with internal ref to CSV 
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    Exemple of multiline requirement.
    Second line: defs:iref[TOTO-0001], defs:iref[TOTO-0123].
    --
    please see defs:iref[TOTO-0002]
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum,Internal links
    requirement,TOTO-0001,"Exemple of multiline requirement.
    Second line: defs:iref[TOTO-0001], defs:iref[TOTO-0123].",~059b7188,"TOTO-0001
    TOTO-0123
    TOTO-0002"
    """
    And the stdout should not contain anything

  Scenario: Extract one definition with attributes to CSV 
    Given a file named "toto.adoc" with:
    """
    :attr-verifiedby-prefix: Verified by:
    :attr-criticity-prefix: Criticity:
    [define, requirement, TOTO-0001]
    One single line.

    something else.
    defs:attribute[verifiedby, Beautiful Test]
    defs:attribute[criticity, Very cool]
    """
    When I successfully run `defmastership export toto.adoc`
    And the stdout should not contain anything
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum,Verified by:,Criticity:
    requirement,TOTO-0001,One single line.,~554b0ff5,Beautiful Test,Very cool
    """
    And the stdout should not contain anything

  Scenario: Extract one definition with missing attributes to CSV
    Given a file named "toto.adoc" with:
    """
    :attr-verifiedby-prefix: Verified by:
    :attr-criticity-prefix: Criticity:
    [define, requirement, TOTO-0001]
    One single line.

    something else.
    defs:attribute[verifiedby, Beautiful Test]
    """
    When I successfully run `defmastership export toto.adoc`
    And the stdout should not contain anything
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum,Verified by:,Criticity:
    requirement,TOTO-0001,One single line.,~554b0ff5,Beautiful Test,
    """
    And the stdout should not contain anything

  Scenario: Extract one definition with example in it
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    [example]
    ----
    [define, requirement, TOTO-002]
    not a real definition
    ----
    --
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum
    requirement,TOTO-0001,"[example]
    ----
    [define, requirement, TOTO-002]
    not a real definition
    ----",~8f20a443
    """
    And the stdout should not contain anything

  Scenario: Extract one definition with single line comment in it
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    // comment
    --
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum
    requirement,TOTO-0001,// comment,~b0b506c3
    """
    And the stdout should not contain anything

  Scenario: Extract one definition with multi line comment or literal in it
    Given a file named "toto.adoc" with:
    """
    [define, requirement, TOTO-0001]
    --
    ////
    multiline comment
    ////
    ....
    literal
    ....
    --
    """
    When I successfully run `defmastership export toto.adoc`
    Then the file "toto.csv" should contain:
    """
    Type,Reference,Value,Checksum
    requirement,TOTO-0001,"////
    multiline comment
    ////
    ....
    literal
    ....",~7eb3c490
    """
    And the stdout should not contain anything
