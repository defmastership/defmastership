Feature: The modify command
  As a Responsible of definitions for a given document
  In order to manage to applu predefined modifications
  I want defmastership to apply automatic modifications

  Scenario: Changes with one modifier
    Given a file named "modifications.yml" with:
    """
    ---
    :temp-references:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1]
    """
    When I successfully run `defmastership modify --modifications temp-references thedoc.adoc`
    Then the stdout should not contain anything
    And the file "modifications.yml" should contain:
    """
    ---
    :temp-references:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 124
    """
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0123]
    """


  Scenario: Change with wrong modifier
    Given a file named "modifications.yml" with:
    """
    ---
    :temp-references:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1]
    """
    When I run `defmastership modify --modifications wrong-modification thedoc.adoc`
    Then it should fail with:
    """
    error: wrong-modification is not a known modification
    """

  Scenario: Make modifications with yaml file parameter 
    Given a file named "pouet.yml" with:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1]
    """
    When I successfully run `defmastership modify --modifications-file=pouet.yml --modifications toto thedoc.adoc`
    Then the stdout should not contain anything
    And the file "pouet.yml" should contain:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 124
    """
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0123]
    """

  Scenario: Generate a table with changes
    Given a file named "modifications.yml" with:
    """
    ---
    :temp-references:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1]
    [define, requirement, TOTO-TEMP-XXX2]
    """
    When I successfully run `defmastership modify --modifications temp-references --changes-summary=changes.csv thedoc.adoc`
    Then the stdout should not contain anything
    And the file "changes.csv" should contain:
    """
    Modifier,Was,Becomes
    temp-references,TOTO-TEMP-XXX1,TOTO-0123
    temp-references,TOTO-TEMP-XXX2,TOTO-0124
    """

  Scenario: Apply two modifcations
    Given a file named "modifications.yml" with:
    """
    ---
    :temp-references:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    :tata_or_titi:
      :type: change_ref
      :config:
        :from_regexp: "(?<tata_or_titi>TATA|TITI)-TEMP-[X\\d]{4}"
        :to_template: "%<tata_or_titi>s-%<next_ref>07d"
        :next_ref: 432
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1]
    [define, requirement, TITI-TEMP-XXX2]
    """
    When I successfully run `defmastership modify --modifications temp-references,tata_or_titi --changes-summary=changes.csv thedoc.adoc`
    Then the stdout should not contain anything
    And the file "modifications.yml" should contain:
    """
    ---
    :temp-references:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 124
    :tata_or_titi:
      :type: change_ref
      :config:
        :from_regexp: "(?<tata_or_titi>TATA|TITI)-TEMP-[X\\d]{4}"
        :to_template: "%<tata_or_titi>s-%<next_ref>07d"
        :next_ref: 433
    """
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0123]
    [define, requirement, TITI-0000432]
    """
