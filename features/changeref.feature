Feature: The changeref command
  As a Responsible of definitions for a giventdocument
  In order to make references reliable
  I want defmastership to change reference for asciidoctor documents

  Scenario: Change a definition 
    Given a file named "modifications.yml" with:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1]
    """
    When I successfully run `defmastership modify --modifications toto thedoc.adoc`
    Then the stdout should not contain anything
    And the file "modifications.yml" should contain:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 124
    """
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0123]
    """

  Scenario: Change a definition with explicit checksum and version
    Given a file named "modifications.yml" with:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1(a~12345678)]
    """
    When I successfully run `defmastership modify --modifications toto thedoc.adoc`
    Then the stdout should not contain anything
    And the file "modifications.yml" should contain:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 124
    """
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0123(a~12345678)]
    """

  Scenario: Change two definitions
    Given a file named "modifications.yml" with:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1]
    [define, requirement, TOTO-TEMP-XXX2]
    """
    When I successfully run `defmastership modify --modifications toto thedoc.adoc`
    Then the stdout should not contain anything
    And the file "modifications.yml" should contain:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 125
    """
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0123]
    [define, requirement, TOTO-0124]
    """

  Scenario: Change definitions with variable from regexp
    Given a file named "modifications.yml" with:
    """
    ---
    :tata_or_titi:
      :type: change_ref
      :config:
        :from_regexp: "(?<tata_or_titi>TATA|TITI)-TEMP-[X\\d]{4}"
        :to_template: "%<tata_or_titi>s-%<next_ref>07d"
        :next_ref: 432
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TITI-TEMP-XXX2]
    [define, requirement, TATA-TEMP-X2X3]
    """
    When I successfully run `defmastership modify --modifications tata_or_titi thedoc.adoc`
    Then the stdout should not contain anything
    And the file "modifications.yml" should contain:
    """
    ---
    :tata_or_titi:
      :type: change_ref
      :config:
        :from_regexp: "(?<tata_or_titi>TATA|TITI)-TEMP-[X\\d]{4}"
        :to_template: "%<tata_or_titi>s-%<next_ref>07d"
        :next_ref: 434
    """
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TITI-0000432]
    [define, requirement, TATA-0000433]
    """

  Scenario: Change definitions in two files
    Given a file named "modifications.yml" with:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1]
    """
    And a file named "thedoc2.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX2]
    """
    When I successfully run `defmastership modify --modifications toto thedoc.adoc thedoc2.adoc`
    Then the stdout should not contain anything
    And the file "modifications.yml" should contain:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 125
    """
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0123]
    """
    And the file "thedoc2.adoc" should contain:
    """
    [define, requirement, TOTO-0124]
    """

  Scenario: Change a internal refs
    Given a file named "modifications.yml" with:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1]
    is the same as defs:iref[TOTO-TEMP-XXX2] and defs:iref[TOTO-TEMP-XXX1]
    [define, requirement, TOTO-TEMP-XXX2]
    is the same as defs:iref[TOTO-TEMP-XXX1]
    """
    When I successfully run `defmastership modify --modifications toto thedoc.adoc`
    Then the stdout should not contain anything
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0123]
    is the same as defs:iref[TOTO-0124] and defs:iref[TOTO-0123]
    [define, requirement, TOTO-0124]
    is the same as defs:iref[TOTO-0123]
    """

  Scenario: Change definitions and iref in two files
    Given a file named "modifications.yml" with:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    """
    And a file named "thedoc.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX1]
    defs:iref[TOTO-TEMP-XXX2]
    """
    And a file named "thedoc2.adoc" with:
    """
    [define, requirement, TOTO-TEMP-XXX2]
    defs:iref[TOTO-TEMP-XXX1]
    """
    When I successfully run `defmastership modify --modifications toto thedoc.adoc thedoc2.adoc`
    Then the stdout should not contain anything
    And the file "thedoc.adoc" should contain:
    """
    [define, requirement, TOTO-0123]
    defs:iref[TOTO-0124]
    """
    And the file "thedoc2.adoc" should contain:
    """
    [define, requirement, TOTO-0124]
    defs:iref[TOTO-0123]
    """

  Scenario: Do not change definitions when in literal
    Given a file named "modifications.yml" with:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    """
    And a file named "thedoc.adoc" with:
    """
    ....
    [define, requirement, TOTO-TEMP-XXX1]
    ....
    """
    When I successfully run `defmastership modify --modifications toto thedoc.adoc`
    Then the stdout should not contain anything
    And the file "modifications.yml" should contain:
    """
    ---
    :toto:
      :type: change_ref
      :config:
        :from_regexp: TOTO-TEMP-[X\d]{4}
        :to_template: TOTO-%<next_ref>04d
        :next_ref: 123
    """
    And the file "thedoc.adoc" should contain:
    """
    ....
    [define, requirement, TOTO-TEMP-XXX1]
    ....
    """
