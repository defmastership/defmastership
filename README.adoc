= defmastership

== Features

* CSV export of https://gitlab.com/defmastership/asciidoctor-defmastership[asciidoctor-defmastership] statements.
* Automatic modifications of asciidoc files for : (See https://defmastership.gitlab.io/defmastership-example/defmastership-example.pdf/#_automatic_modifications[list of modifications].)
** Computation of definitions reference ids
** Explicit checksums updates
** Explicit version updates (note: defmastership-examples not updated with last feature modification)

NOTE: depend on https://gitlab.com/defmastership/defmastership-core[defmastership-core]

== Development method

* BDD:
** https://gitlab.com/defmastership/defmastership/-/tree/master/features[features].
** https://defmastership.gitlab.io/defmastership/features_results.html[features_results.html]

* TDD:
** https://gitlab.com/defmastership/defmastership/-/tree/master/spec/unit/defmastership?ref_type=heads[unit tests (rspec)]
** https://defmastership.gitlab.io/defmastership/rspec_results.html[rspec_results.html]
** https://defmastership.gitlab.io/defmastership/coverage[Unit tests coverage]


